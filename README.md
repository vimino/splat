# Splat

Simple game with both a 2016 and 2020 (responsive) versions.
Find out more [here](https://vimino.gitlab.io/read/making-a-splat/)

## Play the [original here (1 file)](https://vimino.gitlab.io/splat/origin)
## Play the [latest here (3 files)](https://vimino.gitlab.io/splat)