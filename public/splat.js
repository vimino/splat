'use strict'
/*
Copyright 2016-2020 Vítor "VIMinO" Martins

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Game State

let Game = {
    level: 0,
    wait: true,
    over: false,
    next: false,
    score: {base:0, total: 0},
    cheat: false,
}

let Entity = {
    point: [],
    goal: null,
    enemy: [],
    player: null,
    splat: null,
}

// Entities

const Player = function(x=0, y=0, w=30, h=30, speed=5) {
    //let player = Circle('#bbeebb', [x, y, w], '#22bb22', 2)
    //player.w = w
    //player.h = h
    
    let player = Rectangle('#bbeebb', [x, y, w, h], '#22bb22', 2)
    player.speed = speed
    player.follow = 0
    player.target = {x:x,y:y}
    return player
}

const Enemy = function(x=0, y=0, w=30, h=30, direction=1, speed=1) {
    let enemy = Rectangle('#eebbbb', [x, y, w, h], '#ff7777', 2)
    enemy.direction = direction // 1 = up, -1 = down
    enemy.speed = speed
    return enemy
}

const Point = function(x=0, y=0, r=10) {
    return Circle('#eeeebb', [x, y, r], '#cccc22', 2)
}

const Goal = function(x=0, y=0, r=20) {
    return Circle('#eeeebb', [x, y, r], '#cccc22', 2)
}

// Game

class Splat extends Splatter {
    constructor() {
        super()
        bindAll(Splat, this)

        // Keyboard state
        this.key = {}

        // Events
        window.addEventListener('keydown', this.keyboard)
        window.addEventListener('keyup', this.keyboard)
        window.addEventListener('keypress', this.keyboard)
        
        window.addEventListener('mousedown', this.mouse)
        window.addEventListener('mouseup', this.mouse)
        window.addEventListener('mousemove', this.mouse)

        // Start Game
        this.reset()
    }

    mouse(event) {
        if (Game.over || Game.next) {
            if (event.type === 'mousedown') { this.reset() }
            return
        }

        if (!event.which && event.button) {
            if (event.button & 1) { event.which = 1 } // Left
            if (event.button & 4) { event.which = 2 } // Middle
            if (event.button & 2) { event.which = 3 } // Right
        }

        let raw = {x: event.clientX, y: event.clientY}
        let x = (raw.x - this.offset.x + window.pageXOffset) * this.scale.x
        let y = (raw.y - this.offset.y + window.pageYOffset) * this.scale.y

        let pl = Entity.player
        if (event.type === 'mousedown') {
            pl.follow = 1
            pl.target = {x: x, y: y}
        } else if (event.type === 'mouseup') {
            Entity.player.follow = 0
        } else if (event.type === 'mousemove') {
            if (pl.follow == 1) {
                pl.target = {x: x, y: y}
            }
        }
    }

    reset() {
        if(Game.over) {
            Game.level = 0
            Game.score.base = 0
            Game.score.total = 0
        } else {
            Game.score.base = Game.score.total
        }

        if (!Entity.player) { Entity.player = new Player() }
        if (!Entity.splat) { Entity.splat = new Circle('#bbeebb', [0, 0, 20], '#22bb22', 2 )}
        if (!Entity.goal) { Entity.goal = new Goal() }
        Game.over = false
        Game.next = false
        Entity.point = []
        Entity.enemy = []
        this.key = {}
        this.start()
    }

    start() {
        Game.level++
        let level = Game.level

        const cw = this.canvas.width
        const ch = this.canvas.height
 
        const border = {w:cw * 0.1, h:ch * 0.3}
        const space = {w:cw * 0.8, h:ch * 0.4}
        
        // Add, Show and distribute Enemies at random heights
        let total = level
        let step = space.w / (total + 1)
        for (let i = 1; i <= total; ++i) {
            if (i < Entity.enemy.length) {
                Entity.enemy[i].visible = true
            } else {
                Entity.enemy.push(new Enemy()) 
            }

            const e = Entity.enemy[i-1]
            e.place(border.w + (step*i), Random.float(border.h, border.h + space.h))
            e.speed = Random.float(1, 3)
            e.direction = Random.bool() ? 1 : -1
        }

        // Add, Show and distribute Points at random heights
        total = level * 2
        step = space.w / (total + 1)
        for (let i = 1; i <= total; ++i) {
            if (i < Entity.point.length) {
                Entity.point[i].visible = true
            } else {
                Entity.point.push(new Point())
            }

            const p = Entity.point[i-1]
            p.place(border.w + (step * i) + (p.r*1.5), Random.float(border.h, border.h + space.h))
        }

        // Hide and Reset the Splat
        Entity.splat.visible = false

        // Show and Reset the Player position
        Entity.player.place(border.w / 2 - Entity.player.w / 2, ch / 2)
        Entity.player.follow = 0
        Entity.player.target.x = Entity.player.x
        Entity.player.target.y = Entity.player.y
        Entity.player.visible = true
        Entity.player.speed = 5

        // Show and Move the Goal to the ending position
        Entity.goal.visible = true
        Entity.goal.place(border.w + space.w + border.w / 2, ch / 2)
    }

    logic() {
        if (Game.over || Game.next) { return }
        
        const si = {w:this.size.width, h:this.size.height}
        const pl = Entity.player

        // Move Player towards 'target'
        
        if (pl.follow > 0) {
            const myc = {x: pl.x + pl.w/2, y: pl.y + pl.h/2}
            const dist = {x: Math.abs(myc.x - pl.target.x), y: Math.abs(myc.y - pl.target.y)}

            if (dist.x > pl.speed * this.scale.x) {
                if (myc.x < pl.target.x) { pl.x += pl.speed }
                else if (myc.x > pl.target.x) { pl.x -= pl.speed }
            }

            if (dist.y > pl.speed * this.scale.y) {
                if (myc.y < pl.target.y) { pl.y += pl.speed }
                else if (myc.y > pl.target.y) { pl.y -= pl.speed } 
            }
        }

        if (pl.follow > 0 && pl.touches(pl.target)) {
            pl.follow = 0
            Game.over = true
            Entity.splat.place(Entity.player.x + Entity.player.w/2, Entity.player.y + Entity.player.h/2)
            Entity.player.visible = false
            Entity.splat.visible = true
        }

        // Apply Restrictions
        
        if (pl.x < 0) { pl.x = 0 }
        if (pl.x + pl.w > si.w) { pl.x = si.w - pl.w }

        if (pl.y < 0) { pl.y = 0 }
        if (pl.y + pl.h > si.h) { pl.y = si.h - pl.h }

        // Move Enemies

        const base_speed = (Math.log(Math.ceil(Game.level/2)) * 5)
        const barrier = si.h/3 - Game.level/2
        const focus = {min: pl.y - barrier, max: pl.y + barrier }        

        Entity.enemy.forEach(function(e, i) {
            var di = e.direction
            const sp = e.speed + base_speed * (i / Entity.enemy.length)
            if (Game.slow) {
                sp /= 2
                if (sp < 1) { sp = 1 }
            }

            e.move(0, di * sp)

            if ((e.y <= 0 || e.y >= si.h - e.h) ||
                (e.y >= 0 && e.y < focus.min && di < 0) ||
                (e.y <= si.h - e.h && e.y > focus.max && di > 0)) {
                di = -di
            }

            e.direction = di
        })

        // Test Collisions

        Entity.enemy.forEach(function(e) {
            if (e.visible && pl.touches(e)) {
                Entity.splat.place(Entity.player.x + Entity.player.w/2, Entity.player.y + Entity.player.h/2)
                Entity.splat.visible = true
                pl.visible = false
                Game.over = true
            }
        })

        Entity.point.forEach(function(p) {
            if (p.visible && pl.touches(p)) {
                Entity.player.speed += 0.05
                Game.score.total += Game.level
                p.visible = false
            }
        })

        if (Entity.goal.visible && pl.touches(Entity.goal)) {
            Game.score.total += 10 * Game.level
            Entity.goal.visible = false
            Game.next = true
        }
    }

    render() {
        const xt = this.context
        const cw = this.canvas.width
        const ch = this.canvas.height

        xt.clearRect(0, 0, cw, ch)
        xt.save()

        // Background
        this.draw(Rectangle('#555588'))

        // Borders
        this.draw(Rectangle('#444488',[0, 0, cw*0.1, ch])) // left
        this.draw(Rectangle('#444488',[0, 0, cw, ch*0.1])) // up
        this.draw(Rectangle('#444488',[0, ch*0.9, cw, ch])) // right
        this.draw(Rectangle('#444488',[cw*0.9, 0, cw, ch])) // down

        // Draw Player/Splat under everything
        this.draw(Entity.player)
        this.draw(Entity.splat)
        this.draw(Entity.goal)

        // Draw every visible Entity
        Object.keys(Entity).forEach((group) => {
            const g = Entity[group]
            if (g instanceof Array) {
                g.forEach((element) => {
                    this.draw(element)
                })
            //} else if (typeof(g) === 'object') {
            //    this.draw(g)
            }
        })

        // Increase Score to match the Total (especially noticeable on Goals)
        if (Game.score.base < Game.score.total) {
            let shift = Math.trunc((Game.score.total - Game.score.base) / 10)
            if (shift < 1) { shift = 1 }
            Game.score.base += shift
            if (Game.score.base > Game.score.total) {
                Game.score.base = Game.score.total
            }
        }

        // Draw UI

        const fc = '#ffffff'
        const sc = '#333366'
        const lw = 10
        const hw = this.size.width/4

        if (Game.over || Game.next) {
            let col = ['#ffaaaa', '#663333']
            let text1 = 'Game Over'
            if (Game.next) {
                col = [fc, sc]
                text1 = 'Level ' + Game.level + ' Complete'
            }
            this.draw(Label(text1, col[0], [hw, this.size.height/6], col[1], 60, lw))

            let text2 = 'Score: ' + Game.score.base
            this.draw(Label(text2, fc, [hw, this.size.height/3], sc, 40, lw))

            let text3 = ''
            if (!Game.over) {
                if (Game.level < 5) {
                    text3 = 'So far so good.'
                } else if (Game.level < 10) {
                    text3 = "Don't look back!"
                } else if (Game.level < 15) {
                    text3 = 'Mad skills!'
                } else if (Game.level < 20) {
                    text3 = 'Gotta go fast!'
                } else if (Game.level < 25) {
                    text3 = 'Andale! ¡Andale! Arriba! ¡Arriba!'
                } else if (Game.level < 30) {
                    text3 = 'Turbocharged'
                } else if (Game.level < 35) {
                    text3 =  "Can't touch this!"
                } else if (Game.level < 40) {
                    text3 = 'Sonic Speed!'
                } else if (Game.level < 45) {
                    text3 = 'Ludicrous Speed'
                } else if (Game.level < 50) {
                    text3 = 'You Win'
                } else if (Game.level >= 50) {
                    if (Game.level % 2 === 0) {
                        text3 = 'The End'
                    } else {
                        text3 = 'Is Never'
                    }
                } 
            } else if (Entity.splat.visible) {
                text3 = 'Splat!'
            }

            if (text3.length > 0) {
                this.draw(Label(text3, fc, [this.size.width/4, this.size.height/2.5], sc, 40, lw))
            }
        } else { // Draw a Score Counter on top
            this.draw(Label(Game.score.base.toString(), fc, [hw, this.size.height/35], sc, 40, lw))
        }

        xt.restore()
    }

    loop() {
        this.logic()
        super.loop()
    }
}

let splat = new Splat()
splat.loop()
