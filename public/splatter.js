'use strict'
/*
Copyright 2016-2020 Vítor "VIMinO" Martins

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Utilities

const bindAll = function(parent, what) {
    // Keep 'this' consistant by binding every method or declare them as arrow functions:
    //  this.method = this.method.bind(this)
    //  this.method = (args) => { ... }

    let proto = parent.prototype
    const keys = Object.getOwnPropertyNames(proto)
    for (let id in keys) {
        const method = keys[id]
        if (method != 'constructor' && typeof(proto[method]) === 'function') {
                proto[method] = proto[method].bind(what)
        }
    }
}

const isIn = function(what, where) {
    if (where instanceof Array) {
        for (let i = 0; i < where.length; ++i) {
            if (where[i] === what) {
                return true
            }
        }
        return false
    } else if (typeof(where) === 'string') {
        return (where.indexOf(what) >= 0)
    }
}

class Random {
    static bool() { //[false,true]
        var a = Math.random()
        return (a >= 0.5)
    }

    static float(min=0, max=1) {
        let val = Math.random()
        if (typeof(min) === 'undefined') { //[0..1{
            return val
        } else if (typeof(max) === 'undefined') { //[0..min[
            return (val * min)
        } else if (max > min) { // [min..max{
            return ((val * (max-min)) + min)
        } else { //[min, min+max[
            console.warn('Warning@Random.float(' + min + ', ' + max + '): Max is lower than Min')
            return ((val * max) + min)
        }
    }

    static int(min=0, max=1) {
        return Math.floor(Random.float(min, max))
    }
}

// Shapes

const Shape = function(type) {
    let shape = {type: type}

    shape.place = function(x, y) {
        if (x) { shape.x = x }
        if (y) { shape.y = y }
    }

    shape.move = function(x, y) {
        if (x) { shape.x += x }
        if (y) { shape.y += y }
    }

    shape.visible = true

    return shape
}

//class Shape {
//    constructor (type) {
//        this.type = type
//    }   
//}

const Rectangle = function(fill, size, stroke, linewidth) {
    let rect = new Shape('rectangle')

    if (fill) { rect.fs = fill }

    if (size) {
        const l = size.length
        if (l > 0) { rect.x = size[0] }
        if (l > 1) { rect.y = size[1] }
        if (l > 2) { rect.w = size[2] }
        if (l > 3) { rect.h = size[3] }
    }

    if (stroke) { rect.ss = stroke }

    if (linewidth) { rect.lw = linewidth }

    rect.touches = function(what) {
        if (what === undefined) { return false }

        const wa = what
        const ve = { // vertice
            tl: {x: rect.x, y: rect.y},
            tr: {x: rect.x + rect.w, y: rect.y},
            bl: {x: rect.x, y: rect.y + rect.h},
            br: {x: rect.x + rect.w, y: rect.y + rect.h}
        }

        if (what.type === 'rectangle') {
            // what has x,y,w,h

            // check if any vertice touches it
            if (wa.touches(ve.tl) ||
                wa.touches(ve.tr) ||
                wa.touches(ve.bl) ||
                wa.touches(ve.br)
               ) {
                return true
            }
        } else if (what.type === 'circle') {
            // what has x,y,r
            
            let cl = {x: -1, y: -1} // closest vertice

            if (wa.x < ve.tl.x) {
                cl.x = ve.tl.x
            } else if (wa.x > ve.br.x) {
                cl.x = ve.br.x
            } else {
                cl.x = wa.x
            }

            if (wa.y < ve.tl.y) {
                cl.y = ve.tl.y
            } else if (wa.y > ve.br.y) {
                cl.y = ve.br.y
            } else {
                cl.y = wa.y
            }

            let di = {x: 0, y: 0, d: 0} // distance
            di.x = Math.abs(wa.x - cl.x)
            di.y = Math.abs(wa.y - cl.y)
            di.d = Math.sqrt(di.x*di.x + di.y*di.y)

            return (di.d < wa.r)
        } else {
            // what has x,y

            if ((wa.x >= rect.x) &&
                (wa.x <= rect.x + rect.w) &&
                (wa.y >= rect.y) &&
                (wa.y <= rect.y + rect.h)) {
                return true
            }
        }

        return false
    }
  
    return rect
}

const Rec = Rectangle

const Circle = function(fill, size, stroke, linewidth) {
    let circ = new Shape('circle')

    if (fill) { circ.fs = fill }

    if (size) {
        const l = size.length
        if (l > 0) { circ.x = size[0] }
        if (l > 1) { circ.y = size[1] }
        if (l > 2) { circ.r = size[2] }
    }

    if (stroke) { circ.ss = stroke }

    if (linewidth) { circ.lw = linewidth }

    circ.touches = function(what) {
        if (what === undefined) { return false }

        const wa = what

        if (what.type === 'rectangle') {
            // what has x,y,w,h

            const ve = { // vertice
                tl: {x: wa.x, y: wa.y},
                tr: {x: wa.x + wa.w, y: wa.y},
                bl: {x: wa.x, y: wa.y + wa.h},
                br: {x: wa.x + wa.w, y: wa.y + wa.h}
            }

            // check if any vertice touches it
            if (
                circ.touches(ve.tl) ||
                circ.touches(ve.tr) ||
                circ.touches(ve.bl) ||
                circ.touches(ve.br)
            ) {
                return true
            }
        } else if (what.type === 'circle') {
            // what has x,y,r

            let di = [
                wa.x - circ.x,
                wa.y - circ.y,
                wa.r + circ.r
            ]

            return (di[0]*di[0] + di[1]*di[1] < di[2]*di[2])
        } else {
            // what has x,y

            let di = [
                wa.x - circ.x,
                wa.y - circ.y,
                circ.r
            ]

            return (di[0]*di[0] + di[1]*di[1] <= di[2]*di[2])
        }

        return false  

        // http://cgp.wikidot.com/circle-to-circle-collision-detection
    }

    return circ
}

const Label = function(text, fill, position, stroke, size = 10, linewidth = 1, font = 'bold sans-serif') {
    let labe = new Shape('label')

    if (text) { labe.tx = text }

    if (fill) { labe.fs = fill }

    if (position) {
        const l = position.length
        if (l > 0) { labe.x = position[0] }
        if (l > 1) { labe.y = position[1] }
    }

    if (stroke) { labe.ss = stroke }

    if (size) { labe.si = size }

    if (linewidth) { labe.lw = linewidth }

    if (font) { labe.fo = font }

    return labe
}

const Cir = Circle

// Engine

class Splatter {
    constructor(width=800,height=600) {
        this.size = {width:800,height:600}
        this.scale = {x:0,y:0}
        this.offset = {x:0,y:0}
        this.page = {width:0,height:0}
        this.canvas = null
        this.context = null // Canvas Context
    
        this.canvas = document.createElement('canvas')
        this.canvas.width = this.size.width
        this.canvas.height = this.size.height
        this.canvas.style.position = 'relative'
        this.canvas.style.width = this.canvas.width + 'px'
        this.canvas.style.height = this.canvas.height + 'px'

        this.context = this.canvas.getContext('2d')

        bindAll(Splatter, this)

        // Events
        //window.addEventListener('resize', this.resize)

        window.addEventListener('contextmenu', (event) => {
            event.preventDefault()
            event.stopPropagation()
        })

        //window.addEventListener('error', (event) => {
        //    alert('Error(' + event.lineno + ',' + event.colno + '): ' + event.message)
        //})

        document.body.appendChild(this.canvas)
        this.resize()
    }

    resize() {
        const iw = window.innerWidth
        const ih = window.innerHeight

        if (this.page.width != iw || this.page.height != ih) { // Window changed, must Resize
            const cw = this.canvas.width
            const ch = this.canvas.height
            const sw = iw/cw
            const sh = ih/ch

            const style = this.canvas.style

            if (sw < sh) {
                style.width = cw * sw + 'px'
                style.height = ch * sw + 'px'
            } else if (sh < sw) {
                style.width = cw * sh + 'px'
                style.height = ch * sh + 'px'
            }
   
            const rh = parseInt(style.height, 10) // get the number without the leading 'px'
            if (rh < ih)  { // If there's space below the Canvas
                style.marginTop = (ih-rh)/2 + 'px'
            } else {
                style.marginTop = 0
            }

            this.page.width = iw
            this.page.height = ih
            this.offset.x = this.canvas.offsetLeft
            this.offset.y = this.canvas.offsetTop

            // Requires that the Element is in the DOM tree, otherwise = 0,0
            this.scale.x = this.size.width / this.canvas.offsetWidth
            this.scale.y = this.size.height / this.canvas.offsetHeight
        }
    }

    draw(what) {
        if (what === undefined || what === null || ('visible' in what && !what.visible)) {
            return
        }

        const xt = this.context
        xt.save()

        const x = what.x ? what.x : 0
        const y = what.y ? what.y : 0 

        xt.translate(x, y)

        if (isIn(what.type, ['rec', 'rect', 'rectangle'])) {
            const w = what.w ? what.w : this.canvas.width
            const h = what.h ? what.h : this.canvas.height

            if (what.fs) { // Fill Style
                xt.fillStyle = what.fs
                xt.fillRect(0, 0, w, h)
            }

            if (what.ss) { // Stroke Style
                if (what.lw) { // Line Width
                    xt.lineWidth = what.lw ? what.lw : 1
                }

                xt.strokeStyle = what.ss
                xt.strokeRect(0, 0, w, h)
            }
        } else if (isIn(what.type, ['cir', 'circ', 'circle'])) {
            const r = what.r ? what.r/2 : 0
            const pi2 = Math.PI * 2 // Double PI

            if (what.fs) { // Fill Color
                xt.fillStyle = what.fs
                xt.beginPath()
                xt.arc(0, 0, what.r, 0, pi2, false)
                xt.fill()
            }

            if (what.ss) { // Stroke Color
                if (what.lw) { // Line Width
                    xt.lineWidth = what.lw ? what.lw : 0
                }

                xt.strokeStyle = what.ss
                xt.beginPath()
                xt.arc(0, 0, what.r, 0, pi2, false)
                xt.stroke()
            }
        } else if (isIn(what.type, 'label')) {
            xt.textAlign = what.ta ? what.ta : 'center'
            xt.textBaseline = what.bl ? what.bl : 'middle'
            xt.lineJoin = what.lj ? what.lj : 'round'
            
            xt.lineWidth = what.lw ? what.lw : 1
            xt.font = (what.si ? what.si : 10) + 'px' + (what.fo ? ' ' + what.fo : ' bold sans-serif')

            if (what.ss) {
                xt.strokeStyle = what.ss
                xt.strokeText(what.tx, what.x, what.y, this.size.width)
            }

            if (what.fs) {
                xt.fillStyle = what.fs
                xt.fillText(what.tx, what.x, what.y, this.size.width) 
            }
        }

        xt.restore()
    }

    render() {
        const xt = this.context
        const cw = this.canvas.width
        const ch = this.canvas.height

        xt.clearRect(0, 0, cw, ch)
        xt.save()

        //xt.fillStyle = 'white'
        //xt.fillRect(0, 0, cw, ch)
        this.draw({type:'rect',fs:'white'})

        xt.restore()
    }

    loop() {
        this.resize()
        this.render()
        window.requestAnimationFrame(this.loop)
    }
}
